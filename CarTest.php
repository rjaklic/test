<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require_once 'models/Car.php';
require_once 'common.php';

final class CarTest extends TestCase
{
    public $db = null;

    public function __construct()
    {
        global $create_car_table_sql;
        global $create_buyer_table_sql;

        $this->db = new SQLite3('test_cars.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);
        $this->db->enableExceptions(true);

        try {
            if (!$this->db->query('drop table if exists car')) {
                exit(1);
            }

            if (!$this->db->query('drop table if exists buyer')) {
                exit(1);
            }

            if (!$this->db->query($create_car_table_sql)) {
                exit(1);
            }
            if (!$this->db->query($create_buyer_table_sql)) {
                exit(1);
            }
        } catch (Exception $e) {
            print_r($e);
            $this->fail('Cound not drop/create tables.');
        }

        parent::__construct();
    }

    public function testAddCar(): void
    {
        $data = [
            "VehicleID" => 1,
            "InhouseSellerID" => (int) 1,
            "BuyerID" => (int) 2,
            "ModelID" => (int) 3,
            "SaleDate" => '2012-12-12',
            "BuyDate" => '2013-12-14'
        ];

        $car = new Car($this->db);

        $status = $car->insert($data);
        $this->assertTrue($status);

        try {
            $data = [];
            $statement = $this->db->prepare("select * from car where vehicle_id = 1");
            $result = $statement->execute();

            while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
                $data[] = $row;
            }

            $result->finalize();

            $this->assertEquals(1, count($data));
        } catch (Exception $e) {
            print_r($e);
            $this->fail('Cound not get data.');
        }
    }

    public function testAddSameCar(): void
    {
        $data = [
            "VehicleID" => 1,
            "InhouseSellerID" => (int) 1,
            "BuyerID" => (int) 2,
            "ModelID" => (int) 3,
            "SaleDate" => '2012-12-12',
            "BuyDate" => '2013-12-14'
        ];

        $car = new Car($this->db);

        $status = $car->insert($data);
        $this->assertFalse($status);
    }
}
