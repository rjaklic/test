<?php
$create_car_table_sql = 'create table "car" ("vehicle_id" integer primary key, "inhouse_seller_id" integer, "buyer_id" integer, "model_id" integer, "sale_date" text, "buy_date" text);';
$create_buyer_table_sql = 'create table "buyer" ("id" integer primary key, "first_name" text, "last_name" text);';

function fill_with_almost_random_data()
{
    $start_id = 376142;
    $i = 0;

    $car = new Car();

    while ($i < 200) {
        $car->insert([
            'VehicleID' => $start_id,
            'InhouseSellerID' => 82,
            'BuyerID' => 111,
            'ModelID' => 47,
            'SaleDate' => '2014-10-01',
            'BuyDate' => '2020-01-01'
        ]);

        $start_id++;
        $i++;
    }

    $i = 0;
    while ($i < 200) {
        $car->insert([
            'VehicleID' => $start_id,
            'InhouseSellerID' => 82,
            'BuyerID' => 111,
            'ModelID' => 84,
            'SaleDate' => '2014-03-01',
            'BuyDate' => '2020-01-01'
        ]);

        $start_id++;
        $i++;
    }
}