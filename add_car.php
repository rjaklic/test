<?php
require_once 'config.php';
require_once 'common.php';
require_once 'models/Car.php';

function menu()
{
    system('clear');
    echo "=== Menu ===\n";
    echo "1. Add a car\n";
    echo "2. Exit\n";
}

function add_car_menu()
{
    while (false !== ($line = fgets(STDIN))) {
        echo "Vehicle ID: ";

    }
}

function read_input()
{
    while (false !== ($line = fgets(STDIN))) {
        return trim(preg_replace('/$\s\s+/', '', $line));
    }
}

menu();

while (false !== ($line = fgets(STDIN))) {
    $line = trim($line);

    if ($line === "2") {
        system('clear');
        exit(0);
    }

    if ($line === "1") {
        system('clear');
        echo "=== Adding new car ===\n";
        echo "Vehicle ID: ";
        $vehicleId = trim(fgets(STDIN));
        echo "Inhouse Seller ID: ";
        $inhouseSellerId = trim(fgets(STDIN));
        echo "Buyer ID: ";
        $buyerId = trim(fgets(STDIN));
        echo "Model ID: ";
        $modelId = trim(fgets(STDIN));
        echo "Sale date (YYYY-MM-DD): ";
        $saleDate = trim(fgets(STDIN));
        echo "Buy date (YYYY-MM-DD): ";
        $buyDate = trim(fgets(STDIN));
        echo "Confirm (y/n): ";
        $confirm = trim(fgets(STDIN));

        if ($confirm === "y") {
            $data = [
                "VehicleID" => (int) $vehicleId,
                "InhouseSellerID" => (int) $inhouseSellerId,
                "BuyerID" => (int) $buyerId,
                "ModelID" => (int) $modelId,
                "SaleDate" => $saleDate,
                "BuyDate" => $buyDate
            ];

            $car = new Car();
            if (! $car->insert($data)) {
                echo "Error inserting car.";
                exit(1);
            }
        }

        menu();
    }
}

$db->close();
