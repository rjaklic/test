<?php
class Car
{
    private $db = null;

    public function __construct($db = null)
    {
        if ($db === null) {
            global $db;
            $this->db = $db;
        } else {
            $this->db = $db;
        }
    }

    public function insert($car)
    {
        try {
            $statement = $this->db->prepare('insert into car ("vehicle_id", "inhouse_seller_id", "buyer_id", "model_id", "sale_date", "buy_date") values (?, ?, ?, ?, ?, ?)');
            $statement->bindValue(1, $car['VehicleID']);
            $statement->bindValue(2, $car['InhouseSellerID']);
            $statement->bindValue(3, $car['BuyerID']);
            $statement->bindValue(4, $car['ModelID']);
            $statement->bindValue(5, $car['SaleDate']);
            $statement->bindValue(6, $car['BuyDate']);
            $statement->execute();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public function getBestSellingModelPerClient()
    {
        $data = [];

        try {
            $statement = $this->db->prepare("select inhouse_seller_id, model_id, max(c) as total from (select model_id, inhouse_seller_id, count(*) as c from car group by inhouse_seller_id, model_id) group by inhouse_seller_id order by total desc;");
            $result = $statement->execute();

            while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
                $data[] = $row;
            }

            $result->finalize();
        } catch (Exception $e) {
            return false;
        }

        return $data;
    }

    private function countCars($cars_per_month)
    {
        $count = 0;

        foreach ($cars_per_month as $m) {
            $count = $count + $m['count'];
        }

        return $count;
    }

    public function getBestIn3Months()
    {
        // select count(model_id) as c_mid, model_id, month from (select model_id, strftime('%Y-%m', sale_date) as month from car) group by model_id, month order by month, c_mid desc;
        // select max(c_mid), model_id, month from (select count(model_id) as c_mid, model_id, month from (select model_id, strftime('%Y-%m', sale_date) as month from car) group by model_id, month order by month, c_mid desc) group by month;

        $data = [];

        try {
            $statement = $this->db->prepare("select max(c_mid) as count, model_id, month from (select count(model_id) as c_mid, model_id, month from (select model_id, strftime('%Y-%m', sale_date) as month from car) group by model_id, month order by month, c_mid desc) group by month");
            $result = $statement->execute();

            while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
                $data[] = $row;
            }

            $result->finalize();
        } catch (Exception $e) {
            return false;
        }

        $groups = [];

        $group = [];
        foreach ($data as $d) {
            $group[] = $d;

            if (count($group) === 3) {
                $groups[] = $group;
                array_shift($group);
            }
        }

        $best_ones = [];
        foreach ($groups as $g) {
            if (count($g) === 3) {
                $model_ids = [];
                foreach ($g as $model) {
                    $model_ids[] = $model['model_id'];
                }

                if (count(array_count_values($model_ids)) === 1) {
                    $best_ones[] = $g;
                }
            }
        }

        $the_one = null;
        $the_ones = [];
        foreach ($best_ones as $b) {
            if ($the_one === null) {
                $the_one = $b;
                continue;
            }

            if ($this->countCars($b) > $this->countCars($the_one)) {
                $the_ones = [$b];
                $the_one = $b;
            } elseif ($this->countCars($b) === $this->countCars($the_one)) {
                $the_ones[] = $b;
                $the_one = $b;
            }
        }

        return $the_ones;
    }
}
