<?php
require_once 'config.php';
require_once 'common.php';
require_once 'models/Car.php';

function show_data($data)
{
    if (count($data) > 0) {
        echo "Model\tCount\tMonth\n";
    } else {
        echo "No best one for 3 month in a row, lets fill with some almost random data.\n";
        fill_with_almost_random_data();
    }

    foreach ($data as $d) {
        $total_count = 0;

        foreach ($d as $d) {
            $total_count = $total_count + $d['count'];
            echo  "{$d['model_id']}\t{$d['count']}\t{$d['month']}\n";
        }

        echo "Total:\t{$total_count}\n";
        echo "-----------------------\n";
    }
}

$current_file = __FILE__;
echo "Executing \e[0;31;42m{$current_file}\e[0m\n";

$car = new Car();

echo "=== Best selling model in three months in a row ===\n";
$data = $car->getBestIn3Months();
if ($data === false) {
    exit(1);
}
show_data($data);

echo "\n";

echo "=== Best selling model in three months in a row ===\n";
$data = $car->getBestIn3Months();
if ($data === false) {
    exit(1);
}
show_data($data);

$db->close();

echo "Finished.\n";
echo "\n";
