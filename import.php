<?php
require_once 'config.php';
require_once 'common.php';
require_once 'models/Car.php';

function parse_content($content)
{
    $data = [];
    $data_keys = explode(",", trim(array_shift($content)));

    foreach ($content as $c) {
        $tmp = [];
        $line_data = explode(",", trim($c));

        for ($i = 0; $i < count($line_data); $i++) {
            $tmp[$data_keys[$i]] = $line_data[$i];
        }

        $data[] = $tmp;
    }

    return $data;
}

function import_data($data)
{
    global $db;
    global $first_names;
    global $last_names;
    global $create_buyer_table_sql;
    global $create_car_table_sql;

    if (!$db->query('drop table if exists car')) {
        exit(1);
    }

    if (!$db->query('drop table if exists buyer')) {
        exit(1);
    }

    if (!$db->query($create_car_table_sql)) {
        exit(1);
    }
    if (!$db->query($create_buyer_table_sql)) {
        exit(1);
    }

    $car = new Car();

    foreach ($data as $d) {
        if (! $car->insert($d)) {
            exit(1);
        }

        $first_name = $first_names[array_rand($first_names)];
        $last_name = $last_names[array_rand($last_names)];
        $statement = $db->prepare('insert or ignore into buyer("id", "first_name", "last_name") values (?, ?, ?)');
        $statement->bindValue(1, $d['BuyerID']);
        $statement->bindValue(2, $first_name);
        $statement->bindValue(3, $last_name);
        $statement->execute();
    }
}

$current_file = __FILE__;
echo "Executing \e[0;31;42m{$current_file}\e[0m\n";

$url = "https://admin.b2b-carmarket.com//test/project";
$content = explode("<br>", file_get_contents($url));

if (empty($content)) {
    exit(1);
}

echo "Importing data.\n";
$data = parse_content($content);
import_data($data);
$db->close();

echo "Finished.\n";
echo "\n";
