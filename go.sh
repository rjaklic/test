#!/bin/bash

rm -rf ./vendor
php import.php

if [[ $? -eq 0 ]]; then
    php best_selling_model_per_client.php
fi

if [[ $? -eq 0 ]]; then
    php best_in_3_months_in_a_row.php
fi

composer require --dev phpunit/phpunit ^7
./vendor/bin/phpunit --bootstrap vendor/autoload.php CarTest
