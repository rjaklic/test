<?php
require_once 'config.php';
require_once 'common.php';
require_once 'models/Car.php';

$current_file = __FILE__;
echo "Executing \e[0;31;42m{$current_file}\e[0m\n";
echo "=== Best selling model per client ===\n";

$car = new Car();
$data = $car->getBestSellingModelPerClient();
if ($data === false) {
    exit(1);
}
if (count($data) > 0) {
    echo "Inhouse Seller ID\tModel ID\tTotal\n";
}

foreach ($data as $d) {
    echo "{$d['inhouse_seller_id']}\t\t\t{$d['model_id']}\t\t{$d['total']}\n";
}

echo "Finished.\n";
echo "\n";